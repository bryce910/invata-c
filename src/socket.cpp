// TODO(Bryce910): Fix the static_cast<char *>&struct issues; 
#include "socket.hpp"

using namespace invata;

invata_socket::invata_socket()
{
    
}

invata_socket::~invata_socket()
{
    
}

invata_socket::invata_socket(unsigned int port, const std::string &ip_address, TYPE invata_socket_type)
{
    m_current_ip_address = ip_address;
    m_current_port = port;
    m_socket_type = invata_socket_type;
    p_construct();
}

void invata_socket::set_address(const std::string &ip_address)
{
    m_current_ip_address = ip_address;
}


void invata_socket::set_port(unsigned int port)
{
    m_current_port = port;
    p_construct();
}

bool invata_socket::p_construct()
{
    int opt = 1;
    int status = 0, max_clients = 30, sd = 0, max_sd = 0, activity = 0;
    int client_sockets[30] = {0};//30 max connections at a time
    status = socket(AF_INET, SOCK_STREAM, 0);
    fd_set readfds;//socket descriptors
    if(status < 0)
    {
        return false;
    }
    if(setsockopt(status, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0)
    {
        std::cout << "couldn't set opt";
    }
    bzero(reinterpret_cast<char *>(&m_server_socket), sizeof(m_server_socket));
    m_server_socket.sin_family = AF_INET;
    m_server_socket.sin_port = htons(m_current_port);
    m_server_socket.sin_addr.s_addr = INADDR_ANY;
    if(bind(status, reinterpret_cast<struct sockaddr *>(&m_server_socket), sizeof(m_server_socket)) > 0)
    {
        return false;
    }
    listen(status, 5); //5 active connections pending at once
    socklen_t client_length = sizeof(m_client_socket);
    
    while(TRUE)
    {
        std::cout << "RUNNIGN";
        FD_ZERO(&readfds);
        FD_SET(status, &readfds);
        max_sd = status;
        for(int i =0; i < max_clients; i++)
        {
            sd = client_sockets[i];
            if(sd > 0)
            {
                FD_SET(sd, &readfds);
                if(sd > max_sd)
                {
                    max_sd = sd;
                }
            }
        }
        //time to wait..... and see if someone connects....
        activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);
        if((activity < 0) && (errno != EINTR))
        {
            utilities::write::write_file("failed to select connection.", "../data/test.txt");
        }
        if(FD_ISSET(status, &readfds))
        {
            int incoming_status = accept(status, reinterpret_cast<struct sockaddr *>(&m_client_socket), &client_length);
            if(incoming_status < 0)
            {
                utilities::write::write_file("failed to accept connection.", "../data/test.txt");
                return false;
            }
            std::cout << "socket fd is " << incoming_status << ": IP address is " << inet_ntoa(m_client_socket.sin_addr) << ": with the port " << ntohs(m_client_socket.sin_port) << std::endl;
            
            // NOTE(Bryce910): WRITING TO SERVER FIRST
            char text[] = "Server is chilling at moment...come back later.";int write_status = write(incoming_status, text, sizeof(text));
            if(write_status < 0)
            {
                utilities::write::write_file("failed to write to connection.", "../data/test.txt");
                std::cout << "failed to write....";
                return false;
            }
            std::cout << "Added item" << std::endl;
            for(int i = 0; i < max_clients; i++)
            {
                if(client_sockets[i] == 0)
                {
                    client_sockets[i] = incoming_status;
                    std::cout << "Added to list of sockets " << i << std::endl;
                    break;
                }
            }
        }
        std::cout << "Running" << std::endl;
        for(int i = 0; i < max_clients; i++)
        {
            
            sd = client_sockets[i];
            if(FD_ISSET(sd, &readfds))
            {
                std::cout << "Running now." << std::endl;
                char buffer[255];
                int read_status = read(sd,buffer, std::strlen(buffer));
                if(read_status < 0)
                {
                    getpeername(sd, reinterpret_cast<struct sockaddr *>(&m_client_socket), &client_length);
                    std::cout << "Host disconnected with ip: " << inet_ntoa(m_client_socket.sin_addr) << " and port: " << ntohs(m_client_socket.sin_port) << std::endl;
                    
                    close(sd);
                    client_sockets[i] = 0;
                    
                    utilities::write::write_file("failed to read connection.", "../data/test.txt");
                    return false;
                }
                else 
                {
                    std::cout << "Read in: " << read_status << std::endl;
                    char* text2  = buffer;
                    
                    
                    int write_status2 = write(sd, text2, std::strlen(text2));
                    
                    if(write_status2 < 0)
                    {
                        utilities::write::write_file("failed to write to connection.", "../data/test.txt");
                        return false;
                    }
                    // TODO(Bryce910): WRITE TO A FILE...FOR HISTORY
                    std::cout << "Wrote back from server... " << std::endl;
                }
            }
            
        }
        
    }
    // TODO(Bryce910): ADD THE WRITE BACK FUNCTION SINCE WE READFILE
    // NOTE(Bryce910): Do we want to add system to log IPs?
    return true;
}