#include "coordinates.hpp"


world::world(coordinate currentLocation)
    : m_currentLocation(currentLocation), m_currentHeading(0) 
{
}
world::world()
{


}
world::~world()
{

}

int world::get_heading(const coordinate& currentLocation, const coordinate& targetLocation)
{  
    return m_currentHeading;
}
int world::get_heading(coordinate currentLocation, coordinate targetLocation)
{
    return m_currentHeading;
}
int world::get_heading(coordinate targetLocation)
{
    return m_currentHeading;
}
coordinate world::get_location()
{
    return m_currentLocation;
}
void world::update_location(coordinate newLocation)
{
    m_currentLocation = newLocation;
}

float world::calculate_distance(coordinate targetLocation)
{

    return 01.f;
}
float world::calculate_distance(coordinate currentLocation, coordinate targetLocation)
{
    
    return 01.f;
}
void world::apply_earth_curvature(bool curvature)
{
    m_circlized = curvature;
}
float world::convert_to_radians(float val)
{
    return 0.0f;
}
float world::convert_to_degrees(float val)
{
    return 0.0f;
}
float world::convert_to_computer_lat(const string &humanLat)
{
    return 0.0f;
}
float world::convert_to_computer_lon(const string &humanLon)
{
    return 0.0f;
}
string world::convert_to_human_lat(long lat)
{
    char direction = 'N';
    if(lat < 0) { direction = 'S'; }
    unsigned int firstLat = abs(lat);
    unsigned int secondLat = abs(static_cast<int>((lat - firstLat)) * 60);
    unsigned int thirdLat = abs(static_cast<int>((lat - firstLat) - (secondLat/60)*3600));  
    std::string f = std::to_string(firstLat) + "%" + std::to_string(secondLat) + "`" + std::to_string(thirdLat) + "\""; 
    string tempString = f.c_str();
    return tempString;
}
string world::convert_to_human_lon(long lon)
{
	return static_cast<string>(" ");
}

// function convertLat(lat)
//             {
//                     var direction = "N";
//                     var firstLat =  parseInt(lat);
//                     if(firstLat < 0)
//                     {
//                         direction = "S";
//                     }
//                     var firstLat2 = Math.abs(firstLat);
//                     var secondPart =  parseInt((lat - firstLat) * 60);
//                     var secondPartReal = Math.abs(secondPart);
//                     var thirdPart = (((lat - firstLat)  - secondPart/60) * 3600).toFixed(2);
//                     var thirdPartReal = Math.abs(thirdPart);
//                     var latString = firstLat2 + '&deg;' + secondPartReal + '\'' + thirdPartReal + '\"' + direction;
//                     return latString;
//             }
//             function convertLon (lon)
//             {
//                     var direction = "E";
//                     var firstLon =  parseInt(lon);
//                     if(firstLon < 0)
//                     {
//                         direction = "W";
//                     }
//                     var firstLon2 = Math.abs(firstLon);
//                     var secondPartLon =  parseInt((lon - firstLon) * 60);
//                     var secondPartReal =Math.abs(secondPartLon);
//                     var thirdPartLon = (((lon - firstLon)  - secondPartLon/60) * 3600).toFixed(2);
//                     var thirdPartReal = Math.abs(thirdPartLon);
//                     var lonString  = firstLon2 + '&deg;' + secondPartReal + '\'' + thirdPartReal + '\"' + direction;
//                     return lonString;
//             }
