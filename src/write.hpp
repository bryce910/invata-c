#ifndef WRITE__NAME
#define WRITE__NAME

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

namespace utilities
{
    class write
    {
        public:
        write();
        ~write();
        static bool write_file(const std::string &message, const std::string &filePath);
        static bool write_file_at_line(unsigned int lineNumber, const std::string &message, const std::string &filePath);
        static bool write_file_at_end(const std::string &message, const std::string &filePath);
        static std::string read_file(const std::string &filePath);
        static std::string  read_file_at_line(unsigned int lineNumber, const std::string &filePath);
        static std::string read_file_at_size(unsigned int buffersize, const std::string &filePath);
        
        protected:
        private:
        
    };
    
}
#endif