#include "window.hpp"


window::window()
{
    
}
window::window(int width, int height, const char* title)
{
    const unsigned char *version; //"BYTE"
    const unsigned char *renderer; //"BYTE"
    if (!glfwInit ()) {
        //TODO integrate in the debugging system
        fprintf (stderr, "ERROR: could not start GLFW3\n");
    }
    
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    m_screen_title = title;
    m_currentWidth = width;
    m_currentHeight = height;
    m_window = glfwCreateWindow(m_currentWidth, m_currentHeight, m_screen_title.c_str(),NULL, NULL);
    
    
    if (!m_window) {
        //TODO integrate into debugging system
        fprintf (stderr, "ERROR: could not open window with GLFW3\n");
        glfwTerminate ();
        // die();
    }
    glfwMakeContextCurrent (m_window);
    //glfwSwapInterval(0); //was trying to turn off vSync
    /* start GLEW extension handler */
    glewExperimental = GL_TRUE;
    glewInit ();
    
    /* get version info */
    renderer = glGetString (GL_RENDERER);
    version = glGetString (GL_VERSION);
    //TODO add in check...if crasher...grab the version # and render data
    printf ("Renderer: %s\n", renderer);
    printf ("OpenGL version supported: %s\n", version);
    glEnable (GL_DEPTH_TEST);
    glDepthFunc (GL_LESS);
    //   glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND); //FOR ALPHA
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //FOR ALPHA
    
    glfwSetJoystickCallback(update_joystick);
    
    frameCounter = 0;
    
}

window::~window()
{
    
}

void window::start_window(bool windowStart)
{
    p_isOpen = windowStart;
}

void window::set_icon(std::string pixels, unsigned int height, unsigned int width)
{
    
    icon_pixels = pixels;
    _icon_height = height;
    _icon_width = width;
    
    icon = new GLFWimage();
    icon->height = _icon_height;
    icon->width  = _icon_height;
    icon->pixels = reinterpret_cast<unsigned char *>(&icon_pixels);
    
    glfwSetWindowIcon(m_window, 1, icon);
    
}

bool window::is_running()
{
    if(p_isOpen)
    {
        return true;
    }
    return false;
}

void window::close()
{
    start_window(false);
    glfwTerminate();
}

void window::start()
{
    start_window(true);
}

GLFWwindow* window::getWindow()
{
    return m_window;
}

void window::update()
{
    check_frame_lock();
    if(!glfwWindowShouldClose(m_window))
    {
        glfwSwapBuffers(m_window);
        glfwPollEvents();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// glClear(GL_COLOR_BUFFER_BIT);
        int width, height;
        glfwGetFramebufferSize(m_window, &width, &height);
        glViewport(0,0, width, height);
    }
    else
    {
        close();
    }
}

bool window::pressed(unsigned int key)
{
    if(glfwGetKey(m_window, key) == GLFW_PRESS)
    {
        return true;
    }
    if((glfwGetKey(m_window, key) == GLFW_PRESS) && (glfwGetKey(m_window, key) == GLFW_PRESS))
    {
        return true;
    }
    return false;
}

bool window::clicked(unsigned int button)
{
    if(glfwGetMouseButton(m_window, button))
    {
        return true;
    }
    return false;
}

bool window::check_for_joystick()
{
    if(glfwJoystickPresent(GLFW_JOYSTICK_1) == 0)
    {
        m_joystick_active = false;
        return false;
    }
    m_joystick_active = true;
    m_joystick_name = glfwGetJoystickName(GLFW_JOYSTICK_1);
    
    return true;
}
float window::get_joystick_position(int joy, AXIS joystickPosition)
{
    int count;
    const float *x = glfwGetJoystickAxes(joy, &count);
    return x[joystickPosition];
}
float window::is_joystick_button_pushed(int joy, BUTTON joystickButton)
{
    int count;
    const unsigned char *x = glfwGetJoystickButtons(joy, &count);
    std::cout << x[joystickButton] << std::endl;
    return x[joystickButton];
}
void window::update_joystick(int joy, int event)
{
    if(event == GLFW_CONNECTED)
    {
        std::cout << "Joystick connected." << std::endl;
    }
    else if(event == GLFW_DISCONNECTED)
    {
        std::cout << "Joystick disconnected." << std::endl;
    }
}
std::string window::get_clipboard()
{
    return glfwGetClipboardString(m_window);
}
void window::set_clipboard(const std::string clipboardString)
{
    glfwSetClipboardString(m_window, clipboardString.c_str()); 
}
void window::max_frame_rate(unsigned int frame_rate_input)
{
    frame_rate = frame_rate_input;
    std::cout << "Frame rate set: " << frame_rate << std::endl;
}
bool window::check_frame_lock()
{
    if(frame_rate == 0)
    {
        return false;
    }
    else
    {
        // TODO(Bryce910): SET SLEEP TO GET TO MAX_FRAME_RATE
        // TODO(Bryce910): FIRST CHECK TO SEE WHAT FRAME RATE IS
        // TODO(Bryce910): IF LESS THAN MAX...RETURN
        // TODO(Bryce910): ELSE SLEEP...THEN RETURN
        current_frame = glfwGetTime();
        frameCounter++;
        std::cout << "Frame: " << frameCounter << std::endl;
        std::cout << "Before Sleep: " << current_frame;
        usleep(250);
        std::cout << "After Sleep: " << previous_frame;
        if(current_frame - previous_frame > 1.0)
        {
            if(frameCounter > frame_rate)
            {
                //std::cout << "sleeping" << std::endl;
                //usleep(100);//is it sleeping though??
                //or we should do it after frame rate check right? ...errr no...that is wrong actually...not sure
                
            }
            if(m_ms_display)
            {
                std::cout << 1000.0/frameCounter << "ms/frame" << std::endl;
            }
            else 
            {
                std::cout << frameCounter << " FPS" << std::endl;
            }
            frameCounter = 0;
            previous_frame += 1.0;
        }
        return true;
    }
}

void window::set_frame_rate_output(bool ms_output)
{
    m_ms_display = ms_output;
}