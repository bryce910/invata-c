#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include "/opt/local/include/GL/glew.h"
#include "/opt/local/include/GLFW/glfw3.h"
//#include "log.hpp"		// error at moment.;
#include "keys.hpp" 
#include "mouse.hpp"
#include <cstdio>
#include <ctime>
#include <time.h>
#include <unistd.h>

class window
{
    public:
    static bool UPDATEJOYSTICK;
    enum AXIS {LEFT = 0, RIGHT = 1};
    enum BUTTON {BUTTONA = 0,BUTTONB = 1, BUTTONY = 2, BUTTONX = 3 };
    window();
    window(int width, int height, const char* title);
    ~window();
    GLFWwindow* getWindow();
    bool is_running();
    void close();
    void start();
    void update();
    bool check_for_joystick();
    bool pressed(unsigned int key);
    bool clicked(unsigned int key);
    float get_joystick_position(int joy, AXIS joystickPosition);
    float is_joystick_button_pushed(int joy, BUTTON joystickButton);
    std::string get_clipboard();
    void set_clipboard(const std::string clipboardString);
    void set_icon(std::string pixels, unsigned int height, unsigned int width);
    void max_frame_rate(unsigned int frame_rate);
    void set_frame_rate_output(bool ms_output);
    protected:
    void start_window(bool startWindow);
    static void update_joystick(int joy, int event);
    unsigned int frame_rate;
    
    private:
    bool p_isOpen, m_joystick_active, m_ms_display;
    GLFWwindow *m_window;
    int m_currentHeight, m_currentWidth, frameCounter;
    double previous_frame, current_frame;
    std::string m_joystick_name;
    std::string m_screen_title;
    std::string icon_pixels;
    int _icon_height, _icon_width;
    bool check_frame_lock();
    GLFWimage* icon;
    //maybe build the input into its own class, and then just import it into this class...or something like that...not quite sure to be honest...yet
    
};
