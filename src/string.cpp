#include "string.hpp"
	
string::string(const char* string)	
    {
	m_size = strlen(string);
	m_buffer = new char[m_size+1];
	memcpy(m_buffer, string, m_size+1);
	m_buffer[m_size] = 0; //null terminator
    }
string::string()
{
    
}
char& string::operator[](unsigned int index)
    {
	    return m_buffer[index];
    }
string::~string()
    {	
	    delete[] m_buffer;
    }
string::string(const string& newVariable)
	: m_size(newVariable.m_size)
    {
	m_buffer = new char[m_size + 1];
	memcpy(m_buffer, newVariable.m_buffer, m_size+1);
    }
const char* string::getBuffer()
{
    return m_buffer;
}
