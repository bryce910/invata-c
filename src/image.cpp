#include "image.hpp"

image::image()
{
    
}
image::image(const std::string &filePath, IMAGETYPE typeOfImage)
:m_filePath(filePath), m_current_type(typeOfImage)
{
    
}
image::~image()
{
    
}
void image::write_image_data(imageFormat *pixelLayout, unsigned int offset)
{
    std::fstream new_file;
    new_file.open(m_filePath);
    if(new_file.is_open())
    {
        //char header[54]; //need a header for the image..maybe?
        for(auto i = m_writingBuffer.begin(); i < m_writingBuffer.end(); i++)
        {
            new_file.write(reinterpret_cast<char *>(&pixelLayout->pixelInfo.r), sizeof(pixelLayout->pixelInfo.r));
            new_file.write(reinterpret_cast<char *>(&pixelLayout->pixelInfo.g), sizeof(pixelLayout->pixelInfo.g));
            new_file.write(reinterpret_cast<char *>(&pixelLayout->pixelInfo.b), sizeof(pixelLayout->pixelInfo.b));
        }
        new_file.close();
    }
    //do we want to shift the buffer
    return;
}

void image::write_image_data()
{
    std::fstream new_file;
    std::cout << m_filePath << std::endl;
    new_file.open(m_filePath);
    if(new_file.is_open())
    {
        //char header[54]; //need a header for the image..maybe?
        //// TODO(Bryce): MAYBE NEED???unsigned int sizeOfPixel = sizeof(image::pixel);
        for(auto i = m_writingBuffer.begin(); i < m_writingBuffer.end(); i++)
        {
            new_file << i->pixelInfo.r << i->pixelInfo.g << i->pixelInfo.b;
            
            /*
            failed attempt..
        new_file.write(reinterpret_cast<char *>(&i->pixelInfo.r), sizeof(i->pixelInfo.r));
        new_file.write(reinterpret_cast<char *>(&i->pixelInfo.g), sizeof(i->pixelInfo.g));
        new_file.write(reinterpret_cast<char *>(&i->pixelInfo.b), sizeof(i->pixelInfo.b));
      */
        }
        new_file.close();
    }
    else 
    {
        std::cout << "Failed to open file....to write..." << std::endl;
    }
    //do we want to shift the buffer
    return;
}
void image::add_pixel_data(imageFormat pixelLayout)
{
    m_writingBuffer.push_back(pixelLayout);
}

bool image::load_image_data(const std::string &filePath, IMAGETYPE typeOfImage)
{
    m_filePath = filePath;
    m_current_type = typeOfImage;
    return process_image_data();
}
bool image::load_image_data()
{
    if(m_filePath == "")
    {
        return false;
    }
    return process_image_data();
}
void image::shift_image_pixel_data(unsigned int dataIndex, imageFormat *pixelLayout)
{
    
}

std::vector<image::imageFormat>& image::get_image_buffer()
{
    return m_imageBuffer;
}

bool image::has_image()
{
    if(m_imageBuffer.empty())
    {
        return false;
    }
    return true;
}

unsigned int image::get_height()
{
    
    return _image_height;
}

unsigned int image::get_width()
{
    return _image_width;
}

std::string image::get_pixel_data()
{
    return m_pixel_data_string;
}

bool image::process_image_data()
{
    std::fstream temp_file;
    temp_file.open(m_filePath);
    if(temp_file.is_open())
    {
        if(m_current_type == BMP)
        {
            char *header = new char[54]; //54 bytes for bmp header
            temp_file.read(header, 54);
            std::cout << header << std::endl;
            int width = *reinterpret_cast<int *>(&header[18]); //width of bitmap
            std::cout << width << std::endl;
            int height = *reinterpret_cast<int *>(&header[22]); //height of bitmap
            
            _image_width = width;
            
            // TODO(Bryce): Need to put in depth bit
            int heightSign = 1;
            if(heightSign < 0)
            {
                heightSign = -1;
            }
            if(height < 0)
            {
                height = height  * -1;
            }
            _image_height = height ;
            std::cout << "Height(not negative hopefully): " <<  height << std::endl;
            int buffer_size = 4 * width * abs(height); //3=(rgb)+1(4 byte padding) * width * height;
            std::cout << buffer_size << std::endl;
            char *new_buffer = new char[buffer_size];
            temp_file.read(new_buffer, buffer_size);
            temp_file.close();
            imageFormat microBuffer;
            m_pixel_data_string = "";
            m_pixel_data_string = new_buffer;
            for(int i =0; i < buffer_size; i+=4)
            {
                microBuffer.pixelInfo.r = static_cast<int>(new_buffer[i] & 0xff); 
                microBuffer.pixelInfo.g = static_cast<int>(new_buffer[i+1]  & 0xff);
                microBuffer.pixelInfo.b = static_cast<int>(new_buffer[i+2]  & 0xff);
                m_imageBuffer.push_back(microBuffer);
                /*m_pixel_data_string << static_cast<char>(microBuffer.pixelInfo.b) << static_cast<char>(microBuffer.pixelInfo.g) << static_cast<char>(microBuffer.pixelInfo.r); */
                //std::cout << static_cast<unsigned int>(new_buffer[i]);// << ":" << static_cast<unsigned int>(new_buffer[i+1]) << ":" << static_cast<unsigned int>(new_buffer[i+2]) << std::endl;
            }
        }
        else
        {
            return false; //will add more types later...
        }
    }
    else
    {
        return false;
    }
    return true;
}
