#ifndef log__NAME
#define log__NAME

#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include "write.hpp"
namespace debug
{
    class log : public utilities::write
    {
        public:
        log();
        ~log();
        enum DEBUGLEVEL { INFO = 0, WARNING = 1, ERROR = 2, FATAL = 3};
        static DEBUGLEVEL displayLevel;
        static int logMessageCounter;
        static void print_message(const std::string &message, DEBUGLEVEL = INFO);
        protected:
        private:
    };
}
#endif