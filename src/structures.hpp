#ifndef STRUCTURE__NAME
#define STRUCTURE__NAME

#include <stdio.h>
#include <iostream>

//COMMONLY USED STRUCTURES
struct vector2i
{
    int x;
    int y;
};
struct vector3i
{
    int x;
    int y;
    int z;
};
struct vector2f
{
    float x;
    float y;
};
struct vector3f
{
    float x;
    float y;
    float z;
};
struct vec3
{
    float x;
    float y;
    float z;
};
struct vec2
{
    float x;
    float y;
};
struct coordinate
{
    double lat;
    double lon;
};

struct object_struc
{
    coordinate position;
    unsigned int speed;
    unsigned int heading;
};

struct packet 
{
    std::string buffer;
    unsigned int size;
};
struct query 
{
    bool result;
    int limit = 10;
    unsigned int size = 0;
    std::string *fullstring;
    std::string *orderby;
    
};
#endif

