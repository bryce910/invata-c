#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <fstream>

namespace loading
{
	class texture
	{
	public:
		texture();
		texture(const std::string &filePath);
		bool load_texture(const std::string &filePath);
		bool load_texture();
		std::vector<float> grab_texture();
		~texture();
	protected:
		bool process_texture();
	private:
		std::vector<float> m_texture_buffer;
		std::string m_file_path;
		bool m_loaded_texture;
	};
}