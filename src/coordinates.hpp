#ifndef WORLD__NAME
#define WORLD__NAME
#include "structures.hpp"
#include "string.hpp"
#include <cmath>

class world
{
public:
    world(coordinate currentLocation);
    world();
    ~world();


    int get_heading(const coordinate& currentLocation, const coordinate& targetLocation);
    int get_heading(coordinate currentLocation, coordinate targetLocation);
    int get_heading(coordinate targetLocation);

    coordinate get_location();

    float calculate_distance(coordinate targetLocation);
    float calculate_distance(coordinate currentLocation, coordinate  targetLocation);

    void apply_earth_curvature(bool curvature);
    void update_location(coordinate newLocation);
    float convert_to_computer_lat(const string &humanLat);
        string convert_to_human_lat(long lat);
protected:
    float convert_to_radians(float val);
    float convert_to_degrees(float val);

    float convert_to_computer_lon(const string &humanLon);

    string convert_to_human_lon(long lon);
private:
    coordinate m_currentLocation;
    unsigned int m_currentHeading;
    string m_latLon;
    
    bool m_circlized;
};
#endif
