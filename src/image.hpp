#ifndef IMAGE_HPP
#define IMAGE_HPP
#include <stdio.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
class image
{
    public:
    enum IMAGETYPE { PNG = 0, JPG = 1, BMP = 2 };
    struct pixel
    {
        int r;
        int g;
        int b;
    };
    struct imageFormat
    {
        int x;
        int y;
        pixel pixelInfo;
    };
    image();
    image(const std::string &filePath, IMAGETYPE  typeOfImage);
    ~image();
    void write_image_data(imageFormat *pixelLayout, unsigned int offset);
    void write_image_data();
    std::vector<imageFormat>& get_image_buffer();
    bool load_image_data(const std::string &filePath, IMAGETYPE typeOfImage);
    bool load_image_data();
    void add_pixel_data(imageFormat pixel);
    void shift_image_pixel_data(unsigned int dataIndex, imageFormat *pixelLayout);
    bool has_image();
    unsigned int get_height();
    unsigned int get_width();
    std::string get_pixel_data();
    
    private:
    bool process_image_data();
    std::vector<imageFormat> m_imageBuffer;
    std::vector<imageFormat> m_writingBuffer;
    std::string m_pixel_data_string;
    unsigned int _image_width;
    unsigned int _image_height;
    std::string m_filePath;
    IMAGETYPE m_current_type;
};
/*
Bitmap Color Math
10110111
00001101
--------
00000101
*/
#endif