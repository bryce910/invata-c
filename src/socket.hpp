// NOTE(Bryce910): Not sure how this is going to go...new socket code to hopefully help us get cross platform on sockets.
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string>
#include "structures.hpp"
#include "log.hpp"
#define TRUE 1

namespace invata 
{
    class invata_socket
    {
        public:
        
        enum TYPE { TCP, UDP, RAW };
        invata_socket();
        ~invata_socket();
        invata_socket(unsigned int port, const std::string &ip_address, TYPE socket_type);
        
        void set_port(unsigned int port);
        void set_address(const std::string &ip_address);
        
        protected:
        
        bool p_construct();
        
        private:
        
        TYPE m_socket_type;
        unsigned int m_current_port;
        std::string m_current_ip_address, m_current_buffer;
        struct sockaddr_in m_client_socket, m_server_socket;
        
        
    };
    
    
    
    
    
}



