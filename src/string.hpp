#include <stdio.h>
#include <stdlib.h>
#include <iostream>
class string
{
    private:
    char* m_buffer;
    int m_size;
public:
    string(const char* string);
    string();
    ~string();
    char& operator[](unsigned int index);
    string(const string& newVariable);
    const char* getBuffer();
    friend std::ostream& operator<<(std::ostream& stream, const string& string)
	{
	    stream << string.m_buffer; 
	    return stream;
	}
    // friend string operator+(const char *mainString, const char *secondString)
    // {
    // 	unsigned int newLength = sizeof(mainString) + sizeof(secondString) + 1;
    // 	char *s = new char[newLength]; // 
    //     strcat(s,mainString);
    // 	strcat(s,secondString);
    // 	return s;
    // }
    friend bool operator==(const string &mainString, const string &secondString)
    {
	//TODO if second string is longer...it will return true...hmmmmm
	if(mainString.m_size != secondString.m_size)
	{
	    return false;
	}
	for(int i = 0; i < mainString.m_size; i++)
	{
	    if((mainString.m_buffer[i] != secondString.m_buffer[i]))
	    {
		return false;
	    }
	}
	return true;
    }
    friend string operator+(const string &mainString, const string &secondString)
    {
	//unsigned int newLength = mainString.m_size + secondString.m_size + 1;
        strcat(mainString.m_buffer, secondString.m_buffer);	
	return mainString.m_buffer;
    }
};
