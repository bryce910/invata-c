#include "input.hpp"

input::input()
{
    m_killSwitch = true;
    run();
}
input::~input()
{

}
void input::run()
{
    std::thread m_threading(&input::getKeyUp, this);
    m_threading.join();
}
void input::stop()
{
    m_killSwitch = true;
}
void input::getKeyUp()
{
   	std::getline(std::cin, m_string);
	std::cout << "why" << m_string << std::endl;
}
void input::getKeyDown(bool &killSwitch)
{
    killSwitch = true;
}
void input::getStringInput(bool &killSwitch)
{
    killSwitch = true;
}
void input::get_string_input(std::string &buffer, bool &finished)
{
    input::turnCanOn();
    std::getline(std::cin, buffer);
    finished=true; 
}
void input::get_input(char &a, bool &done)
{
    struct termios old = {0};
    if(tcgetattr(0, &old) < 0)
    {
	debug::log::print_message("Failed, it is less than 0", debug::log::FATAL);
    }
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0)
    {
	debug::log::print_message("Failed, it is less than 0", debug::log::FATAL);
    }
    if (read(0, &a, 1) < 0) //Using stdin to save input char to BUF
    {
	debug::log::print_message("Failed, it is less than 0", debug::log::FATAL);
    }
    done = true;
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0)
    {
	debug::log::print_message("Failed, can't reset0", debug::log::FATAL); 
    }
//    return (buf);
}

void input::turnCanOn()
{
    struct termios old = {0};
    if(tcgetattr(0, &old) < 0)
    {
	debug::log::print_message("Failed, it is less than 0", debug::log::FATAL);
    }
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0)
    {
	debug::log::print_message("Failed, can't reset0", debug::log::FATAL); 
    }
}
