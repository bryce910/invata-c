#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "structures.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <istream>
#include <string>
#include "image.hpp"

class object
{
    public:
    enum OBJECTYPE { BUFFER, COLOR, NORMAL, TEXTURE};
    enum FILETYPE { OBJ, CHAR };
    enum OBJGROUPS { V, VT, VN, F};
    object();
    object(const std::string &filePath);
    object(const std::string &filePath, FILETYPE objectClassification);
    void set_object_type(FILETYPE objectClassification);
    bool load_file();
    bool load_file(const std::string &filePath);
    bool load_texture(image &loadedTexture);
    bool apply_texture( image &loadedTexture);
    
    unsigned int get_size();
    std::vector<vec3>& get_vertex_buffer();
    ~object();
    protected:
    bool process_load(const std::string &filePath);
    private:
    std::vector<vec3> m_verticie_buffer;
    std::vector<vec2> m_vt_buffer;
    std::vector<vec3> m_vn_buffer;
    std::vector<vec3> m_f_buffer;
    std::vector<vec3> m_v_index, m_uv_index, m_n_index;
    std::vector<vec3> m_verticie_color;
    std::vector<vec3> m_verticie_normal;
    std::vector<vec3> m_verticie_texture;
    std::vector<vec3> out_m_verticie;
    std::string m_filepath;
    OBJECTYPE m_currentObjectType;
    FILETYPE m_currentFileType;
    unsigned int m_buffer_size;
    bool process_obj_file();
    void process_f_line(const std::string &buffer, vec3 &vertexIndex, vec3 &uvIndex, vec3 &normalIndex);
    vec3 process_v_line(const std::string &buffer);
    vec3 process_vn_line(const std::string &buffer);	
    vec2 process_vt_line(const std::string &buffer);
    bool process_texture();
};

