#include "write.hpp"


using namespace utilities;


write::write()
{

}
write::~write()
{

}

bool write::write_file(const std::string &message, const std::string &filePath)
{
    std::ofstream tempFile;
    tempFile.open(filePath);
    if(tempFile.is_open())
    {
	tempFile << message << std::endl;
	tempFile.close();
    }
    return false;
}
bool write::write_file_at_line(unsigned int lineNumber, const std::string &message, const std::string &filePath)
{
    std::ifstream tempFile;
    tempFile.open(filePath);
    std::vector<std::string> fileBuffer;
    std::string oldbuffer;
    if(tempFile.is_open())
    {
	while(std::getline(tempFile, oldbuffer))
	{
	    fileBuffer.push_back(oldbuffer);
	}
	tempFile.close();
    }
    if(!fileBuffer.empty())
    {
	std::ofstream newFile;
	newFile.open(filePath);
	if(newFile.is_open())
	{
	    for(auto i = fileBuffer.begin(); i != fileBuffer.end(); ++i)
	    {
		if(i - fileBuffer.begin() == lineNumber)
		{
		    newFile << message << std::endl;
		}
		newFile << *i << std::endl;	 
	    }
	    newFile.close();
	    return true;
	}
    }
    return false;
}
bool write::write_file_at_end(const std::string &message, const std::string &filePath)
{
    std::ofstream tempFile;
    tempFile.open(filePath, std::ios::app);
    if(tempFile.is_open())
    {
	tempFile << message << std::endl;
	tempFile.close();
	return true;
    }
    return  false;
}
std::string  write::read_file(const std::string &filePath)
{
    std::ifstream tempFile;
    std::string tempBuffer, buffer;
    tempFile.open(filePath);
    if(tempFile.is_open())
    {
	while(std::getline(tempFile, tempBuffer))
	{
	    buffer += tempBuffer;
	}
	tempFile.close();
    }
    return buffer;
}
std::string write::read_file_at_line(unsigned int lineNumber, const std::string &filePath)
{
    unsigned int currentLine = 0;
    std::fstream tempFile;
    std::string buffer;
    tempFile.open(filePath);
    if(tempFile.is_open())
    {
	while(getline(tempFile, buffer))
	{
	    if(currentLine == lineNumber)
	    {
		tempFile.close();
		return buffer;
	    }
	    currentLine++;
	}
	tempFile.close();
    }
    return "Error....file is not big enough...please check the intended line number";
}
std::string write::read_file_at_size(unsigned int buffersize, const std::string &filePath)
{
    std::ifstream tempFile;
    tempFile.open(filePath);
    std::string tempBuffer;
    char buffer[buffersize];
    if(tempFile.is_open())
    {
	tempFile.read(buffer, buffersize);
	tempFile.close();
    }
    //Added the null terminator in the end of the buffer
    buffer[buffersize] = '\0';
    return static_cast<std::string>(buffer);
}
