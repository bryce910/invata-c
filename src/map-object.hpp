/*
New File
*/

class map_object
{
    public:
    
    map_object();
    ~map_object();
    
    void set_border(unsigned int border_key, double x, double y);
    void remove_border(unsigned int border_key);
    
    void update_border(unsigned int border_key, double x, double y);
    
    
    private:
    
    std::vector<std::vector<double>> _objectBorder;
    bool _active;
    unsigned int _last_key;
}

