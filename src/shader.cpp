#include "./shader.hpp"

shader::shader()
{
    
}
shader::~shader()
{
    
}
shader::shader(SHADERTYPE typeofShader)
:m_shader_type(typeofShader)
{
    
}
void shader::set_shader_type(SHADERTYPE typeofShader)
{
    m_shader_type = typeofShader;
}
bool shader::load_shader(const std::string &shaderFile)
{
    std::fstream m_shader_source;
    std::string temp_line;
    m_shader_file_path = shaderFile;
    //process shader
    m_shader_source.open(m_shader_file_path);
    m_shader_source.seekg(0, std::ios::end);
    size_t fileSize = m_shader_source.tellg();
    temp_line.reserve(fileSize);
    int currentType = 0;
    m_shader_source.seekg(0, std::ios::beg);
    if(m_shader_type == VERTEX)
    {
        vertexSource.resize(fileSize);
        m_shader_source.read(&vertexSource[0], fileSize);
        if(m_shader_source)
        {
        }
    }
    else if(m_shader_type == FRAGMENT)
    {
        fragmentSource.resize(fileSize);
        m_shader_source.read(&fragmentSource[0], fileSize);
    }
    else if(m_shader_type == VERTEXFRAGMENT)
    {
        while(getline(m_shader_source, temp_line))
        {
            if(temp_line.find("#vertex") != std::string::npos)
            {
                currentType = 0;
            }	
            else if(temp_line.find("#fragment") != std::string::npos)
            {
                currentType = 1;
            }
            else
            {
                if(currentType == 0)
                {
                    vertexSource += temp_line + '\n';
                }
                else 
                {
                    fragmentSource += temp_line + '\n';
                }
            }
        }
    }
    m_shader_source.close();
    if(vertexSource != "" || fragmentSource != "")
    {
        return true;
    }
    return false;
}
bool shader::load_shader()
{
    if(load_shader(m_shader_file_path))
    {
        return true;
    }
    return false;
}
void shader::set_shader(const std::string &shaderFile)
{
    m_shader_file_path = shaderFile;
}

unsigned int shader::compile_shader(unsigned int type, const std::string &source)
{
    unsigned int id = glCreateShader(type);
    const char* src = source.c_str(); //&source[0]//MUST BE IN SCOPE
    glShaderSource(id, 1, &src, NULL);
    glCompileShader(id);
    //TODO::ADD ERROR HANDLING
    int result;
    glGetShaderiv(id, GL_COMPILE_STATUS, &result);
    if(!result)
    {
        int length;
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
        char *message = new char[length];
        glGetShaderInfoLog(id, length, &length, message);
        std::cout << "Failed to " << (type == GL_VERTEX_SHADER ? "vertex" : "fragement") << " compille: ";
        std::cout << message << std::endl;
        delete[] message;
        return 0;
    }
    return id;
}
unsigned int shader::create_shader(const std::string& vertexShader, const std::string& fragementShader)
{   
    unsigned int  program = glCreateProgram();
    unsigned int vs = compile_shader(GL_VERTEX_SHADER, vertexShader);
    unsigned int fs = compile_shader(GL_FRAGMENT_SHADER, fragementShader);
    
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);
    glValidateProgram(program);
    
    glDeleteShader(vs);
    glDeleteShader(fs);
    
    return program;
}
