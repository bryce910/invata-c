
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <iostream>
#include <vector>
#include <string>
#include <thread>

#include "log.hpp"
class input
{
public:
    input();
    ~input();
    void run();
    void stop();
    static void get_input(char &a, bool &done);
    static void get_string_input(std::string &buffer, bool &done);
    static void turnCanOn();
    std::string getKey();
    static bool ActiveEvent;

protected:
    void getKeyUp();
    void getKeyDown(bool &killSwitch);
    void getKeyInput(bool &killSwitch);
    void getStringInput(bool &killSwitch);
private:
    bool m_killSwitch;
    int m_privateInput;
    std::string m_string;
};

