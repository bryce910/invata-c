#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include "/opt/local/include/GL/glew.h"
#include "/opt/local/include/GLFW/glfw3.h"

class shader 
{
    public:
    enum SHADERTYPE { VERTEX, FRAGMENT, VERTEXFRAGMENT };
    shader();
    shader(SHADERTYPE typeOfShader);
    void set_shader_type(SHADERTYPE typeOfShader);
    bool load_shader(const std::string &shaderFile);
    bool load_shader();
    void set_shader(const std::string &shaderFile);
    static unsigned int create_shader(const std::string &vertexShader, const std::string &fragementShader);
    static unsigned int compile_shader(unsigned int type, const std::string &source);
    std::string vertexSource;
    std::string fragmentSource;
    ~shader();
    protected:
    
    private:
    SHADERTYPE m_shader_type;
    std::string m_shader_file_path;
};