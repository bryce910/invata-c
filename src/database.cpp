#include "database.hpp"

database::database()
{
    
}
database::database(const std::string &databaseName, const std::string &database_user, const std::string &database_password)
{
    m_database_name = databaseName;
    m_user = database_user;
    m_pass = database_password;
}
database::~database()
{
    
}

void database::set_database_name(const std::string &databaseName)
{
    m_database_name = databaseName;
}
void database::set_user(const std::string &database_user)
{
    m_user = database_user;
}
void database::set_pass(const std::string &database_pass)
{
    m_pass = database_pass;
}
void database::query(const std::string &database_query, struct query &results )
{
    // TODO(Bryce910): Will be interesting...time to get this stuff working...
    m_current_query = database_query;
    // TODO(Bryce910): break up time string time?
    int size_of_query = sizeof(m_current_query);
    int from_location = m_current_query.find("FROM");
    int select_location = m_current_query.find("SELECT");
    int update_location = m_current_query.find("UPDATE");
    int delete_location = m_current_query.find("DELETE");
    int insert_location = m_current_query.find("INSERT");
    if(from_location != std::string::npos || (select_location != std::string::npos || update_location !=  std::string::npos || delete_location != std::string::npos || insert_location != std::string::npos ))
    {
        std::cout << m_current_query << std::endl;
        results.size = size_of_query;
        int token_index = 0, token_offset = 0, previous_index;
        char token[]  = "WHERE";
        while((token_index = m_current_query.find(token, token_offset)) != std::string::npos)
        {
            parse_limit_statement(previous_index, token_index, m_current_query);
            std::cout << "found a " << token << " at " << token_index << std::endl;
            token_offset = (token_index + 1);
            previous_index = token_index;
            
        }
    }
    else 
    {
        std::cout << "Not a valid command." << std::endl;
    }
    //results.
}

void database::parse_limit_statement(unsigned int previous_index, unsigned int current_index, std::string &query_string)
{
    //this should now break and search for string after the WHERE
    int token_index_first, token_index_second;
    token_index_first = query_string.find(' ', current_index);
    token_index_second = query_string.find(' ', token_index_first+1);
    if(token_index_second == -1)
    {
        token_index_second = sizeof(query_string);
    }
    std::cout << token_index_second << ":";
    std::cout << token_index_first << std::endl;
    unsigned int token_size = token_index_second - token_index_first;
    char query_limit[token_size-1]; // NOTE(Bryce910): Have to offset so it doesn't grab the actual tokens it is looking to grab between. 
    
    // TODO(Bryce910): Still need to verify memcpy is doing the correct offset...at the moment can't quite tell....seems to be sending me a blank when it should be something else....might work...not sure...
    memcpy(&query_limit, &query_string[token_index_first+1], (token_size-1));
    query_limit[token_size-1] = '\0';
    m_query_key = query_limit;
    std::cout << "Query Command Size: " << token_size <<  " : " << "Limit Query Key: " << m_query_key << std::endl;
}




