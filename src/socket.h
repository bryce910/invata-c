#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <string>
#include "write.hpp"

namespace invata 
{
    class socket
    {
        public:
        
        enum TYPE { TCP, UDP, RAW }
        socket();
        ~socket();
        socket(unsigned int port, const std::string &ip_address, TYPE socket_type);
        
        void set_port(unsigned int port);
        void set_address(const std::string &ip_address);
        void set_packet(packet &socket_packet);
        
        ~socket();
        
        protected:
        
        
        private:
        
        TYPE m_socket_type;
        unsigned int m_current_port;
        std::string m_current_ip_address;
        struct sockaddr_in m_client_socket, m_server_socket;
        bool p_construct();
        
    };
    
}


