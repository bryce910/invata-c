/*
New File
*/
#include "socket.hpp"
#include <thread>
namespace invata
{
    
    class server : public invata_socket
    {
        public:
        server();
        ~server();
        bool start_server();
        bool end_server();
        bool recieved_data();
        bool update_server();
        bool broadcast_too_all();
        bool broadcast(unsigned int user_id);
        std::string get_data();
        private:
        bool running_server();
        bool start_server_loop();
        bool close_server();
        bool m_data_recieved;
        std::string m_recent_data;
        std::thread server_thread;
    };
}
