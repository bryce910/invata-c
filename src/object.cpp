#include "object.hpp"


object::object()
{
    
}
object::object(const std::string &filePath)
:m_filepath(filePath)
{
    
}
object::object(const std::string &filePath, FILETYPE objectClassification)
:m_filepath(filePath), m_currentFileType(objectClassification)
{
    
}
object::~object()
{
    
}
bool object::load_texture(image &loadedTexture)
{
    vec3 pixel;
    std::vector<image::imageFormat> temp_buffer = loadedTexture.get_image_buffer();
    for(auto i = temp_buffer.begin(); i != temp_buffer.end();i++)
    {
        // TODO(BRYCE): Set the texture buffers.. 
        pixel.x = i->pixelInfo.r;
        pixel.y = i->pixelInfo.g;
        pixel.z = i->pixelInfo.b;
        m_verticie_texture.push_back(pixel);
    }
    if(process_texture())
    {
        return true;
    }
    return false;
}

bool object::load_file()
{
    if(process_load(m_filepath))
    {
        return true;
    }
    return false;
}
bool object::load_file(const std::string &filePath)
{
    m_filepath = filePath;
    if(process_load(m_filepath))
    {
        return true;
    }
    return false;
}	
bool object::process_load(const std::string &filePath)
{
    if(m_currentFileType == OBJ)
    {
        if(process_obj_file())
        {
            return true;
        }
        return false;
    }
    std::fstream temp_file;
    std::string temp_buffer;
    temp_file.open(m_filepath);
    if(temp_file.is_open())
    {
        temp_file.seekg(0, std::ios::end);
        m_buffer_size = temp_file.tellg();
        temp_file.seekg(0, std::ios::beg);
        // m_verticie_buffer.resize(m_buffer_size);
        int index = 0;
        vec3 temp_struct; 
        while(getline(temp_file, temp_buffer, ','))
        {
            if(temp_buffer == "#object")
            {
                m_currentObjectType = BUFFER;
                std::cout << "OBJECT FOUND" << std::endl;
            }
            else if(temp_buffer == "#color")
            {
                m_currentObjectType = COLOR;
            }
            else 
            {
                if(index == 0)
                {
                    temp_struct.x = static_cast<float>(std::stof(temp_buffer.c_str()));
                    index++;
                }
                else if(index == 1)
                {
                    temp_struct.y = static_cast<float>(std::stof(temp_buffer.c_str()));	
                    index++;
                }
                else 
                {
                    temp_struct.z = static_cast<float>(std::stof(temp_buffer.c_str()));
                    m_verticie_buffer.push_back(temp_struct);
                    index=0;
                }
            }		
        }
        for(auto ii = m_verticie_buffer.begin(); ii != m_verticie_buffer.end(); ii++)
        {
            std::cout << ii->x << ":" << ii->y << ":" << ii->z << std::endl;
        }
    }
    else
    {
        return false;
    }
    return true;
}

void object::set_object_type(FILETYPE objectClassification)
{
    m_currentFileType = objectClassification;
}
bool object::process_obj_file()
{
    
    std::fstream file;
    std::string temp_buffer, temp_micro_buffer;
    file.open(m_filepath);
    OBJGROUPS currentType = V;
    if(file.is_open())
    {
        while(std::getline(file, temp_buffer))
        {
            if(temp_buffer.find("vt") != std::string::npos)
            {
                if(temp_buffer.find("vt") == 0)
                {
                    currentType = VT;
                    m_vt_buffer.push_back(process_vt_line(temp_buffer));	
                }
                
            }
            else if(temp_buffer.find("vn") != std::string::npos)
            {
                if(temp_buffer.find("vn") == 0)
                {
                    currentType = VN;	
                    m_vn_buffer.push_back(process_vn_line(temp_buffer));
                }
            }
            else if(temp_buffer.find("v") != std::string::npos)
            {
                if(temp_buffer.find("v") == 0)
                {
                    currentType = V;
                    m_verticie_buffer.push_back(process_v_line(temp_buffer));	
                }
            }
            else if(temp_buffer.find("f") != std::string::npos)
            {
                if(temp_buffer.find("f") == 0)
                {
                    currentType = F;
                    vec3 vi;
                    vec3 uvi;
                    vec3 ni;
                    process_f_line(temp_buffer, vi, uvi, ni);
                    m_v_index.push_back(vi);
                    m_uv_index.push_back(uvi);
                    m_n_index.push_back(ni);
                }
            }
        }
    }
    // for(auto i = m_verticie_buffer.begin(); i != m_verticie_buffer.end(); i++)
    // {
    // 	std::cout << i->x << ":" << i->y << ":" << i->z << std::endl;
    // }
    for(auto  i = m_v_index.begin(); i != m_v_index.end(); i++)
    {
        // std::cout << i->x << ":" << i->y << ":" << i->z << std::endl;
    }
    return false;
}
vec3 object::process_v_line(const std::string &buffer)
{
    vec3 temp_vec;
    std::istringstream true_buffer(buffer);
    
    std::string position;
    unsigned int index = 0;
    
    while(std::getline(true_buffer, position, ' '))
    {
        if(index == 1) { temp_vec.x = static_cast<float>(stof(position)); }
        if(index == 2) { temp_vec.y = static_cast<float>(stof(position)); }
        if(index == 3) { temp_vec.z = static_cast<float>(stof(position)); }
        index++;
    }
    return temp_vec;
}
vec2 object::process_vt_line(const std::string &buffer)
{
    vec2 temp_vec;
    std::istringstream true_buffer(buffer);
    std::string position;
    unsigned int index = 0;
    while(std::getline(true_buffer, position, ' '))
    {
        if(index == 1) { temp_vec.x = static_cast<float>(stof(position)); }
        if(index == 2) { temp_vec.y = static_cast<float>(stof(position)); }
        index++;
    }
    return temp_vec;
}
vec3 object::process_vn_line(const std::string &buffer)
{
    vec3 temp_vec;
    std::istringstream true_buffer(buffer);
    
    std::string position;
    unsigned int index = 0;
    
    while(std::getline(true_buffer, position, ' '))
    {
        if(index == 1) { temp_vec.x = static_cast<float>(stof(position)); }
        if(index == 2) { temp_vec.y = static_cast<float>(stof(position)); }
        if(index == 3) { temp_vec.z = static_cast<float>(stof(position)); }
        index++;
    }
    return temp_vec;
}
void object::process_f_line(const std::string &buffer, vec3 &out_vertexIndex, vec3 &out_uvIndex, vec3 &out_normalIndex)
{
    vec3 temp_vec[3];
    std::istringstream true_buffer(buffer);
    std::string position;
    unsigned int index = 0;
    while(std::getline(true_buffer, position, ' '))
    {
        unsigned int second_index = 0;
        std::istringstream second_true_buffer(position);
        std::string second_position;
        if(index != 0)
        {
            while(std::getline(second_true_buffer, second_position, '/'))
            {
                if(second_index == 0) { temp_vec[index-1].x = static_cast<float>(stof(second_position)); }
                if(second_index == 1) { temp_vec[index-1].y = static_cast<float>(stof(second_position)); }
                if(second_index == 2) { temp_vec[index-1].z = static_cast<float>(stof(second_position)); }
                second_index++;	
            }
        }
        index++;
    }
    out_vertexIndex = temp_vec[0];
    out_uvIndex = temp_vec[1];
    out_normalIndex = temp_vec[2];
}
unsigned int object::get_size()
{
    //return 32;
    return m_verticie_buffer.size() * sizeof(vec3);
}
std::vector<vec3>& object::get_vertex_buffer()
{
    for(unsigned int i = 0; i < m_v_index.size(); i++)
    {
        unsigned int vertexIndex = m_v_index[i].x;
        unsigned int vertexIndex2 = m_uv_index[i].x;
        unsigned int vertexIndex3 = m_n_index[i].x;
        // unsigned int vertexIndex2 = m_v_index[i].y;
        // unsigned int vertexIndex3 = m_v_index[i].z;
        out_m_verticie.push_back(m_verticie_buffer[vertexIndex-1]);
        out_m_verticie.push_back(m_verticie_buffer[vertexIndex2-1]);
        out_m_verticie.push_back(m_verticie_buffer[vertexIndex3-1]);
        // out_m_verticie.push_back(m_verticie_buffer[vertexIndex2-1]);
        // out_m_verticie.push_back(m_verticie_buffer[vertexIndex3-1]);
        // vec3 temp_ver = {m_verticie_buffer[vertexIndex-1].x,  m_verticie_buffer[vertexIndex2-1].y, m_verticie_buffer[vertexIndex3-1].z};
        // std::cout << temp_ver.x << ":" << temp_ver.y << ":" << temp_ver.z << std::endl;
        // out_m_verticie.push_back(temp_ver);
        
    }
    return out_m_verticie;
}
bool object::process_texture()
{
    return false;
}
bool object::apply_texture(image &loadedTexture)
{
    //apply image...same as load_texture();
    if(load_texture(loadedTexture))
    {
        return true;
    }
    return false;
}