#include <stdlib.h>
#include <stdio.h>
#include <string> // NOTE(Bryce910): might not need
#include "structures.hpp"

class database
{
    public:
    
    database();
    ~database();
    database(const std::string &databaseName, const std::string &database_user, const std::string &database_password);
    
    bool create_database_name(const std::string &name);
    bool create_database_user(const std::string &username, const std::string &password);
    bool assigned_database_user(const std::string &dabasename, const std::string &username);
    
    void set_database_name(const std::string &databaseName);
    void set_user(const std::string &database_user);
    void set_pass(const std::string &database_pass);
    void query(const std::string &database_query, query &results );
    
    
    protected:
    
    private:
    std::string m_user, m_pass, m_database_name, m_current_query;
    void parse_limit_statement(unsigned int previous_index, unsigned int current_index, std::string &query_string);
    char* m_query_key;
};


