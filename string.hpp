class string
{
    private:
    char* m_buffer;
    int m_size;
public:
    string(const char* string)	
    {
	m_size = strlen(string);
	m_buffer = new char[m_size+1];
	memcpy(m_buffer, string, m_size+1);
	m_buffer[m_size] = 0; //null terminator
    }
    char& operator[](unsigned int index)
    {
	    return m_buffer[index];
    }
    ~string()
    {	
	    delete[] m_buffer;
    }
    string(const string& newVariable)
	: m_size(newVariable.m_size)
    {
	m_buffer = new char[m_size + 1];
	memcpy(m_buffer, newVariable.m_buffer, m_size+1);
    }
    friend std::ostream& operator<<(std::ostream& stream, const string& string)
    {
	    stream << string.m_buffer;
	    return stream;
    }
};
