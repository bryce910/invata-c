#include <stdio.h>
#include <iostream>
#include <vector>
#include <thread>
#include "src/window.hpp"
#include "src/object.hpp"
#include "lib/glm/glm.hpp"
#include "lib/glm/vec3.hpp" // glm::vec3
#include "lib/glm/vec4.hpp" // glm::vec4
#include "lib/glm/mat4x4.hpp" // glm::mat4
#include "lib/glm/gtc/matrix_transform.hpp" // glm::translate, glm::rotate, glm::scale, glm::perspective
#include "src/input.hpp"
#include "src/shader.hpp"
#include "src/image.hpp"
#include "src/coordinates.hpp"
#include "src/server.hpp"
#include "src/database.hpp"
#define DEBUGGING  true


#ifdef __APPLE__ //FOR APPLE STUFF

#define ASSERT(X) if(!(x))return(0);

#define GLCall(x) GLClearError();\
x;\
GLLogCall(#x, __FILE__, __LINE__)

#endif



static void GLClearError()
{
    while(glGetError() != GL_NO_ERROR);
}

static bool GLLogCall(const char* function, const char* file, int line)
{
    while(GLenum error = glGetError())
    {
        std::cout << "OPENGL ERROR" << error << "" << std::endl;
        return false;
    }
    return true;
}

// OPERATE OVERLOADS FOR STRUCTURES
std::ostream& operator<<(std::ostream& stream, vector2f& arg)
{
    stream << arg.x << " : " << arg.y;
    return stream;
}
std::ostream& operator<<(std::ostream& stream, vector3f& arg)
{
    stream << arg.x << ":" << arg.y << ":" << arg.z;
    return stream;
}
std::ostream& operator<<(std::ostream& stream, coordinate& cord)
{
    stream << cord.lat << ":" << cord.lon;
    return stream;
}
char getch();
//NOTE: used to define the logging system
debug::log::DEBUGLEVEL debug::log::displayLevel = debug::log::INFO;
int debug::log::logMessageCounter = 0;
const std::string FILEPATH = "./data/";
bool input::ActiveEvent = true;

// int main(int argc, char **argv)
// {
//     world *earth = new world();
//     coordinate zld = {123.654, -1239.124};
//     earth->update_location(zld);
//     coordinate z = earth->get_location();
//     string xxxxxxx = earth->convert_to_human_lat(-231);
//     std::cout << xxxxxxx << std::endl;
//     string example = "This works";
//     string example1 = "This works";
//     example[1] ='i';		 
//     string example2 = "This works also";
//     if(example == example2)
//     {
// 	std::cout << "true" << std::endl;
//     }
//     else
//     {
// 	std::cout << "failed." << std::endl;
//     }

//     vector2f pos = {1.0f, 2.0f};
//     vector3f pos1 = {1.0f, 2.0f, 3.0f};
//     coordinate currentPosition = {-32.00234098f, 96.129380f};
//     char *xx = new char('a');
//     char **xxx = &xx;
//     std::cout << currentPosition << std::endl;
//     std::cout << xx << std::endl;
//     std::cout << *(xx) << std::endl;
//     std::cout << (*xx) << std::endl;
//     std::cout << *xx << std::endl;
//     std::cout << (*xxx) << std::endl;
//     std::cout << pos << std::endl;
//     std::cout << example << std::endl;
//     std::cout << example2 << std::endl;
//     std::cout << pos1 << std::endl;
//     if(argc > 1)
//     {
// 	std::cout << argv[1] << std::endl;
//     }
//     image testImage("./test.bmp",  image::BMP);
//     if(!testImage.has_image())
//     {
// 	debug::log::print_message("Failed to load image.", debug::log::FATAL);
//     }
//     std::string x = "FAILED";
//     debug::log::print_message("string example", debug::log::INFO);
//     debug::log::print_message(x, debug::log::INFO);
//     debug::log::print_message(x, debug::log::WARNING);
//     debug::log::print_message("Not a winner....", debug::log::FATAL);
//     //Write file done...working perfect
//     debug::log::write_file("Testing this \n", FILEPATH + "./test.txt");
//     //write file to end of file...working perfect
//     debug::log::write_file_at_end("This is the last line.. \n", FILEPATH + "./test.txt");
//     std::cout << "----------------------------------------" << std::endl;
//     debug::log::write_file_at_line(2, "this is going to be inserted in a file \n",  FILEPATH + "./test.txt");
//     //Read file and return buffer...working perfect
//     std::cout << debug::log::read_file(FILEPATH + "./test.txt") << std::endl;
//     std::cout << "----------------------------------------" << std::endl;
//     //Read file at line and return buffer....working perfect
//     std::cout << debug::log::read_file_at_line(2, FILEPATH + "./test.txt") << std::endl;
//     //Read file with max buffer size....working perfect
//     std::cout << "----------------------------------------" << std::endl;
//     std::cout << debug::log::read_file_at_size(2, FILEPATH + "./test.txt") << std::endl;    
//     std::cout << "----------------------------------------" << std::endl;
//     std::cout << "Number of Printed Values Debug Messages: " <<debug::log::logMessageCounter << std::endl;
//     window::window mainWindow;
//     mainWindow.start();
//     std::string liner = "";
//     char p = 0;
//     bool threadComplete = false, chatDone = false;
//     bool chatActive = false;
//     std::vector<std::thread> _inputThreads;
//     std::vector<std::thread> _chatThreads;
//     std::string chatBuffer;   
//     // THREAD LAMBA OPTION std::thread([&]{char = get_input(p, threadComplete); })
//     _inputThreads.push_back(std::thread(input::get_input, std::ref(p), std::ref(threadComplete)));
//     while(mainWindow.is_running())
//     {
// 	if(input::ActiveEvent)
// 	{
// 	    if(threadComplete && !chatActive)
// 	    {
// 		auto &i = _inputThreads.back();
// 		if(i.joinable())
// 		{
// 		    i.join();
// 		}
// 		if(p == 27)	// ASCII CODE FOR ESC : 27
// 		{
//     		    input::ActiveEvent = false;
// 		    mainWindow.close();
// 		}
// 		else if(p == 'c')
// 		{
// 		    p = static_cast<char>(NULL);	// Clears buffer so it doesn't retrigger
// 		    _chatThreads.push_back(std::thread(input::get_string_input, std::ref(chatBuffer), std::ref(chatDone)));
// 		    chatActive = true;
// 		    threadComplete = true;
// 		}
// 		else
// 		{
// 		    _inputThreads.push_back(std::thread(input::get_input, std::ref(p), std::ref(threadComplete)));
// 		    threadComplete = false;
// 		}
// 	    }
// 	    if(chatActive)
// 	    {
// 		if(chatDone)
// 		{
// 		    auto &cB = _chatThreads.back();
// 		    if(cB.joinable())
// 		    {
// 			cB.join();
// 			chatActive = false;
// 			chatDone = false;
// 		    }
// 		}
// 	    }
//        }

//     }
//     return 0;
// }
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_E && action == GLFW_PRESS)
        std::cout << "fart";
}
query x;

int main()
{
    
    std::thread i = std::thread([&]{
                                invata::server *server = new invata::server();
                                server->set_address("127.0.0.1");
                                server->set_port(9812);
                                server->start_server();
                                server->update_server();
                                if(server->recieved_data())
                                {
                                std::string packet_string = server->get_data();
                                }
                                });
    
    std::thread second_window = std::thread([&]{
                                            
                                            
                                            //while(game_second->is_running())
                                            //{ game_second->update(); }
                                            });
    
    // NOTE(Bryce910):  ....don't need to set for server...since this is the server...server->set_address("192.168.1.1");
    packet singlePacket;
    
    singlePacket.buffer = "FART";
    singlePacket.size = sizeof(singlePacket.buffer);
    // NOTE(Bryce910): Just a proof of concept 
    std::cout << singlePacket.buffer << std::endl;
    std::cout << singlePacket.size << std::endl;
    std::cout << "failed to understand what is going on.";
    
    // GLFWwindow* windows = NULL;
    //   const GLubyte* renderer;
    //   const GLubyte* version;
    
    //   if (!glfwInit ()) {
    //     fprintf (stderr, "ERROR: could not start GLFW3\n");
    //     return 1;
    //   }
    window *game = new window(640, 480, "Game");
    window *game_second = new window(640, 480, "Second Window");
    //   glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    //   glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 2);
    //   glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    //   glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    //   windows = glfwCreateWindow (640, 480, "OpenGL", NULL, NULL);
    //   if (!windows) {
    //     fprintf (stderr, "ERROR: could not open window with GLFW3\n");
    //     glfwTerminate ();
    //     return 1;
    //   }
    //   glfwMakeContextCurrent (windows);
    
    //   /* start GLEW extension handler */
    //   glewExperimental = GL_TRUE;
    //   glewInit ();
    
    //   /* get version info */
    //   renderer = glGetString (GL_RENDERER);
    //   version = glGetString (GL_VERSION);
    //   printf ("Renderer: %s\n", renderer);
    //   printf ("OpenGL version supported: %s\n", version);
    
    //   /* tell GL to only draw onto a pixel if the shape is closer to the viewer */
    //   glEnable (GL_DEPTH_TEST);
    //   glDepthFunc (GL_LESS);
    //   // float vertices[6] = {
    //   //    -0.5f, -0.5f,
    //   //     0.0f, 0.5f,
    //   //    0.5f, -0.5f
    //   // };
    //   float points[] = {
    //    0.0f,  0.25f,  0.0f,
    //    0.25f, -0.25f,  0.0f,
    //   -0.25f, -0.25f,  0.0f
    //   };
    //   float points2[] = {
    //    0.9f,  0.9f,  0.0f,
    //    0.5f, 0.9f,  -0.5f,
    //   0.9f, -0.9f,  0.0f
    //   };
    //   float points3[] = {
    //     -0.5f,  0.5f, 1.0f, // Top-left
    //      0.5f,  0.5f, 0.0f, // Top-right
    //      0.5f, -0.5f, 0.0f, // Bottom-right
    
    //      0.5f, -0.5f, 0.0f, // Bottom-right
    //     -0.5f, -0.5f, 1.0f, // Bottom-left
    //     -0.5f,  0.5f, 1.0f,  // Top-left  
    
    //   };
    static const float points4[] = {
        -1.0f,-1.0f,-1.0f, // triangle 1 : begin
        -1.0f,-1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f, // triangle 1 : end
        1.0f, 1.0f,-1.0f, // triangle 2 : begin
        -1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f,-1.0f, // triangle 2 : end
        1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        1.0f, 1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        -1.0f,-1.0f,-1.0f,
        -1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f,-1.0f,
        1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f,
        1.0f,-1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f,-1.0f,-1.0f,
        1.0f, 1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f,-1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f,-1.0f,
        -1.0f, 1.0f,-1.0f,
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f,-1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f,-1.0f, 1.0f
    };
    
    static const GLfloat g_color_buffer_data[] = {
        0.583f,  0.771f,  0.014f,
        0.609f,  0.115f,  0.436f,
        0.327f,  0.483f,  0.844f,
        0.822f,  0.569f,  0.201f,
        0.435f,  0.602f,  0.223f,
        0.310f,  0.747f,  0.185f,
        0.597f,  0.770f,  0.761f,
        0.559f,  0.436f,  0.730f,
        0.359f,  0.583f,  0.152f,
        0.483f,  0.596f,  0.789f,
        0.559f,  0.861f,  0.639f,
        0.195f,  0.548f,  0.859f,
        0.014f,  0.184f,  0.576f,
        0.771f,  0.328f,  0.970f,
        0.406f,  0.615f,  0.116f,
        0.676f,  0.977f,  0.133f,
        0.971f,  0.572f,  0.833f,
        0.140f,  0.616f,  0.489f,
        0.997f,  0.513f,  0.064f,
        0.945f,  0.719f,  0.592f,
        0.543f,  0.021f,  0.978f,
        0.279f,  0.317f,  0.505f,
        0.167f,  0.620f,  0.077f,
        0.347f,  0.857f,  0.137f,
        0.055f,  0.953f,  0.042f,
        0.714f,  0.505f,  0.345f,
        0.783f,  0.290f,  0.734f,
        0.722f,  0.645f,  0.174f,
        0.302f,  0.455f,  0.848f,
        0.225f,  0.587f,  0.040f,
        0.517f,  0.713f,  0.338f,
        0.053f,  0.959f,  0.120f,
        0.393f,  0.621f,  0.362f,
        0.673f,  0.211f,  0.457f,
        0.820f,  0.883f,  0.371f,
        0.982f,  0.099f,  0.879f
    };
    //   unsigned int vbo = 0;
    //   glGenBuffers(1, &vbo);
    //   glBindBuffer(GL_ARRAY_BUFFER, vbo);
    //   glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), points, GL_STATIC_DRAW);
    //   GLuint vao = 0;
    //   glGenVertexArrays(1, &vao);
    //   glBindVertexArray(vao);
    //   glBindBuffer(GL_ARRAY_BUFFER, vbo);
    //   glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    //   glEnableVertexAttribArray(0);	// Index of attribute
    
    //   unsigned int vbo2 = 1;
    //   glGenBuffers(1, &vbo2);
    //   glBindBuffer(GL_ARRAY_BUFFER, vbo2);
    //   glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), points2, GL_STATIC_DRAW);
    
    //     GLuint vao1 = 0;
    //   glGenVertexArrays(1, &vao1);
    //   glBindVertexArray(vao1);
    //   glBindBuffer(GL_ARRAY_BUFFER, vbo2);
    //   glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    //   glEnableVertexAttribArray(0);	// Index of attribute
    //   glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    //    unsigned int vbo3 = 1;
    //   glGenBuffers(1, &vbo3);
    //   glBindBuffer(GL_ARRAY_BUFFER, vbo3);
    //   glBufferData(GL_ARRAY_BUFFER, 18 * sizeof(float), points3, GL_STATIC_DRAW);
    
    //     GLuint vao2 = 0;
    //   glGenVertexArrays(1, &vao2);
    //   glBindVertexArray(vao2);
    //   glBindBuffer(GL_ARRAY_BUFFER, vbo3);
    //   glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    //   glEnableVertexAttribArray(0);	// Index of attribute
    //   glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    unsigned int vbo4 = 1;
    glGenBuffers(1, &vbo4);
    glBindBuffer(GL_ARRAY_BUFFER, vbo4);
    glBufferData(GL_ARRAY_BUFFER, (36*3) * sizeof(float), points4, GL_STATIC_DRAW);
    
    unsigned int  vao3 = 0;
    glGenVertexArrays(1, &vao3);
    glBindVertexArray(vao3);
    glBindBuffer(GL_ARRAY_BUFFER, vbo4);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(0);	// Index of attribute
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    unsigned int colorbuffer;
    glGenBuffers(1, &colorbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);
    
    // 2nd attribute buffer : colors
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
    glVertexAttribPointer(
        1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
        3,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*)0                          // array buffer offset
        );
    //   // Start Location, Number that makes attribute, type....docs.gl for good reference
    //   // glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, 0);
    // // const char* vertex_shader =
    // // "#version 400\n"
    // // "in vec3 vp;"
    // // "void main() {"
    // // "  gl_Position = vec4(vp, 1.0);"
    // // "}";
    // // const char* fragment_shader =
    // // "#version 400\n"
    // // "out vec4 frag_colour;"
    // // "void main() {"
    // // "  frag_colour = vec4(0.5, 0.0, 0.5, 1.0);"
    // // "}";
    //   std::string  vertex_Shader =	// 
    //     "#version 330 core\n"
    //     "\n"
    //     "layout(location = 0) in vec4 position; out vec4 color;\n"
    //     "\n"
    //     "void main()\n"
    //     "{\n"
    //     " gl_Position = position;\n"
    //     " color = vec4(position.x+0.5, 0.12, position.y+0.5, 1);\n"
    //     "}\n";
    
    // std::string  vertexShader =	// 
    //     "#version 330 core\n"
    //     "\n"
    //     "layout(location=0) in vec3 position; \n"
    //     "uniform mat4 finalTransform;\n"
    //     "out vec4 color;\n "
    //     "\n"
    //     "void main()\n"
    //     "{\n"
    //     " gl_Position = finalTransform * vec4(position, 1);\n"
    //     " color = vec4(position.x+0.5, 0.12, position.y+0.5, 1);\n"
    //     "}\n";
    
    // std::string  vertexShader2 =	// 
    //     "#version 330 core\n"
    //     "\n"
    //     "layout(location=0) in vec3 position; \n"
    //     "layout(location=1) in vec3 nColor; \n"
    //     "uniform mat4 finalTransform;\n"
    //     "uniform float alpha, red, green, blue; \n"
    //     "uniform int positionOffset; \n"
    //     "out vec4 color;\n "
    //     "\n"
    //     "void main()\n"
    //     "{\n"
    //     " gl_Position = finalTransform * vec4(position, 1);\n"
    //     " if(positionOffset == 1) { color =  vec4(red, green, blue, alpha); } \n"
    //     " if(positionOffset == 0) { color =  vec4(position.x + red, position.x + green, position.x + blue, alpha); } \n"
    //     "}\n";
    //   std::string  fragementShader =
    //     "#version 330 core\n"
    //     "\n"
    //     " in vec4 color; out vec4 colorout;"
    //     "\n"
    //     "void main()\n"
    //     "{\n"
    //     " colorout = color;\n"
    //     "}\n";
    
    shader menu_shader = shader(shader::VERTEX);
    menu_shader.load_shader("./src/shaders/vertexShader.shader"); 
    menu_shader.set_shader_type(shader::FRAGMENT);
    menu_shader.load_shader("./src/shaders/fragementShader.shader");
    unsigned int shader =menu_shader.create_shader(menu_shader.vertexSource, menu_shader.fragmentSource);
    glUseProgram(shader);
    
    //      int finalTransformUniformLocation = glGetUniformLocation(shader, "finalTransfrom"); // 
    
    //   /* OTHER STUFF GOES HERE NEXT */
    // //  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    // //do{
    //       int xi = 0;
    //       int zi = 0;
    //       int yi = 0;
    //       bool forward = true ;
    //       while(!glfwWindowShouldClose(windows))
    //       {
    // 	  glfwSetKeyCallback(windows, key_callback); // More event style
    // 	  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);// glClear(GL_COLOR_BUFFER_BIT);
    // 	  int width, height;
    // 	  glfwGetFramebufferSize(windows, &width, &height);
    // 	  glViewport(0, 0, width, height);
    // //	  BIND BUFFER      
    // //	  DRAW TRIANGLE
    // 	  glDrawArrays(GL_TRIANGLES, 0, 3); //
    //   	   glBindVertexArray(vao1);
    // 	  glDrawArrays(GL_TRIANGLES, 0, 3);
    // 	  glBindVertexArray(vao2);
    // 	  glDrawArrays(GL_TRIANGLES, 0, 6);
    // 	  glBindVertexArray(vao3);
    
    // 	  /*Modifies the actual object */
    //       glm::mat4 projectionMatrix  = glm::perspective(glm::radians(60.0f), static_cast<float>(width) / static_cast<float>(height), 0.1f, 10.0f);
    //       glm::mat4 translationMatrix = glm::translate(projectionMatrix, glm::vec3(0.0f, 0.0f, -3.0f));
    //       glm::mat4 rotationMatrix = glm::rotate(translationMatrix, glm::radians(34.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    
    //      // Camera matrix
    //       if(xi == 300)
    //       {
    // 	  forward = false;
    //       }
    //       else if(xi == 0)
    //       {
    // 	  forward = true;
    //       }
    //       int state = glfwGetKey(windows, GLFW_KEY_D);
    //       if(state == GLFW_PRESS)
    //       {
    // 	  xi--;			// 
    //       }
    //       state = glfwGetKey(windows, GLFW_KEY_A);
    //       if(state == GLFW_PRESS)
    //       {
    // 	  xi++;
    //       }
    //        state = glfwGetKey(windows, GLFW_KEY_W);
    //       if(state == GLFW_PRESS)
    //       {
    // 	  zi--;			// 
    //       }
    //       state = glfwGetKey(windows, GLFW_KEY_S);
    //       if(state == GLFW_PRESS)
    //       {
    // 	  zi++;
    //       }
    //       rotationMatrix = glm::rotate(translationMatrix, glm::radians(static_cast<float>(xi)), glm::vec3(0.0f, static_cast<float>(zi/10), 1.0f));
    
    //       /*camera */
    //       glm::mat4 View = glm::lookAt(
    // 	  glm::vec3(4,3,3), // Camera is at (4,3,3), in World Space
    // 	  glm::vec3(0,0,0), // and looks at the origin
    // 	  glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
    // 	  );
    //       // Model matrix : an identity matrix (model will be at the origin)
    //       glm::mat4 Model = glm::mat4(1.0f);
    // //           glm::mat4 finalProjection = projectionMatrix  *  View * Model;
    //       glm::mat4 finalProjection = rotationMatrix *  View * Model; //nice working fiew...finalProjection..camera
    // //      glm::mat4 finalProjection = projectionMatrix * translationMatrix * rotationMatrix;
    
    //       int finalTransformUniformLocation = glGetUniformLocation(shader, "finalTransform");
    //       //glUniformMatrix4fv(finalTransformUniformLocation, 1, GL_FALSE, &rotationMatrix[0][0]); //rotate object
    //       glUniformMatrix4fv(finalTransformUniformLocation, 1, GL_FALSE, &finalProjection[0][0]); // Look through camera
    //       glDrawArrays(GL_TRIANGLES, 0, 12*3); //
    //       //END OF FIRST INSTANCE
    //       rotationMatrix = glm::rotate(translationMatrix, glm::radians(static_cast<float>(xi/.10)), glm::vec3(0.0f, 0.0f, 1.0f));
    //       finalProjection = rotationMatrix * View * Model;
    //       glUniformMatrix4fv(finalTransformUniformLocation, 1, GL_FALSE, &finalProjection[0][0]); // Look through camera
    //       glDrawArrays(GL_TRIANGLES, 0, 12*3); //
    //       glBindVertexArray(0);
    //     //UNBIND BUFFER
    //     // Swap buffers
    //     glfwSwapBuffers(windows);
    //     glfwPollEvents();
    // } // Check if the ESC key was pressed or the window was closed
    // //while(glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window) == 0 ); // 
    //   /* close GL context and any other GLFW resources */
    //   glfwTerminate ();
    game->start();
    game->max_frame_rate(30);
    game_second->start();
    game_second->max_frame_rate(30);
    float r = 1, g = 1, b = 1, a = 1, rotationScale = 0; // 1.0 scale   
    debug::log *logger = new debug::log();
    object *ob = new object();
    //TODO = OBJECT LOADER
    // ob->set_object_type(object::CHAR);
    // ob->load_file("./data/main.char");
    ob->set_object_type(object::OBJ);
    //ob->load_file("./data/test_object.obj");
    ob->load_file("./data/Drone_obj.obj");
    
    //  ob->load_file("./data/IronMan.obj");
    glm::vec3 position = {0, 0, 5};
    float horizontalAngle = 3.14;
    float verticalAngle = 0.0f;
    float initialFoV = 45.0f;
    //need to actually build something in the system to use the mouse...nothing works at the moment. 
    float speed = 3.0f;
    float mouseSpeed = 0.0005f;
    
    // //LOAD TEXTURE -- Come back to it
    //  unsigned int textureID;
    //  glGenTextures(1,&textureID);
    //  glBindTexture(GL_TEXTURE_2D,  textureID);
    // LOAD A FILE UP INTO VBO
    unsigned int vbo = 0;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER,  ob->get_vertex_buffer().size() * sizeof(vec3), &ob->get_vertex_buffer()[0], GL_STATIC_DRAW);
    GLuint vao = 4;
    // VAO FOR THIS BUFFER
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    //Textures
    unsigned int textureID =0;
    /*
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bitmap->getWidth(),bitmap->getHeight(), 0, GL_BGR, GL_UNSIGNED_BYTE, bitmap->getTexture());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    */
    //GL stuff
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(0);  // Index of attribute
    
    
    //TODO: the following comments
    //O m    'Finish OBJ loader
    //Load textures
    //Dynamic load / update objects 
    //Dynamic load / update textures 
    double lastTime = 0;
    float FoV = 45;
    if(!game->check_for_joystick())
    {
        std::cout << "you do not have a joystick attached." << std::endl;
    }
    else 
    {
        std::cout << "you do have a joystick" << std::endl;
        float leftStick = game->get_joystick_position(1, window::AXIS::LEFT);
        std::cout << leftStick << std::endl;
    }
    std::cout << game->get_clipboard() << std::endl;
    game->set_clipboard("FART");
    std::cout << game->get_clipboard() << std::endl;
    std::cout << MOUSE::MOUSE_BUTTON_LEFT << std::endl;
    image *bitmap = new image("./data/smurffy.bmp", image::BMP);
    
    image::imageFormat bitMapPixel;
    unsigned int heightIndex=0;
    unsigned int maxHeight=255, maxWidth = 255;
    //bitmap->setSize(maxHeight, maxWidth);
    
    for(int index = 0; index <= maxWidth; index++)
    {
        bitMapPixel.x = index;
        bitMapPixel.y = heightIndex;
        bitMapPixel.pixelInfo.r = index;
        bitMapPixel.pixelInfo.b = index;
        bitMapPixel.pixelInfo.g = index;
        bitmap->add_pixel_data(bitMapPixel);
        if(index == 255 && maxHeight != heightIndex)
        {
            index = 0;
            heightIndex++;
        }
    }
    
    bitmap->load_image_data();
    if(!bitmap->has_image())
    {
        logger->print_message("Failed to load image buffer", debug::log::FATAL);
        //bitmap->write_image_data(); //doesn't write the header or anything for a file
    }
    else 
    {
        logger->print_message("Buffer loaded...ready to apply", debug::log::INFO);
    }
    //bitmap->write_image_data();
    ob->apply_texture(*bitmap);
    
    std::cout << bitmap->get_pixel_data() << std::endl;
    std::cout << bitmap->get_width() << std::endl;
    std::cout << bitmap->get_height() << std::endl;
    game->set_icon(bitmap->get_pixel_data(), bitmap->get_width(), bitmap->get_height());
    database *d = new database("localhost", "bryce910", "blanet910");
    query result;
    d->query("SELECT * FROM users WHERE id = 1", result);
    while(game->is_running())
    {
        game->update();
        game_second->update();
        
        //simple check for mouse click
        if(game->pressed(keyboard::KEY_LEFT_SHIFT) && game->pressed(keyboard::KEY_R))
        {
            //~server();
            std::cout << "Server closed." << std::endl;
        }
        
        
        if(game->clicked(MOUSE::MOUSE_BUTTON_LEFT))
        {
            std::cout << "mouse clicked." << std::endl;
        }
        double currentTime = glfwGetTime();
        float deltaTime = float(currentTime - lastTime);
        lastTime = currentTime;
        double xpos, ypos;
        //glfwGetCursorPos(game->getWindow(), &xpos, &ypos);
        //glfwSetCursorPos(game->getWindow(), 1024/2, 768/2);
        horizontalAngle += mouseSpeed * deltaTime * float(1024/2 - xpos );
        verticalAngle   += mouseSpeed * deltaTime * float( 768/2 - ypos );
        glm::vec3 direction = {
            cos(verticalAngle) * sin(horizontalAngle),
            sin(verticalAngle),
            cos(verticalAngle) * cos(horizontalAngle)
        };
        glm::vec3 right = {
            sin(horizontalAngle - 3.14f/2.0f),
            0,
            cos(horizontalAngle - 3.14f/2.0f)
        };
        glm::vec3 up = glm::cross(right,direction);
        //float FoV = initialFoV - 5 * glfwGetScrollBacj();
        
        if(game->pressed(keyboard::KEY_E) && game->pressed(keyboard::KEY_LEFT_SHIFT))
        {
            glUseProgram(0);
            logger->print_message("KEY E AND SHIFT PUSHED...reloading shaders");
            menu_shader.set_shader_type(shader::VERTEX);
            menu_shader.load_shader("./src/shaders/vertexShader.shader"); 
            menu_shader.set_shader_type(shader::FRAGMENT);
            menu_shader.load_shader("./src/shaders/fragementShader.shader"); 
            unsigned int shader = menu_shader.create_shader(menu_shader.vertexSource, menu_shader.fragmentSource);
            glUseProgram(shader);
        }
        if(game->pressed(keyboard::KEY_SPACE))
        { 
            logger->print_message("SPACE KEY PUSHED...game exited", debug::log::INFO);
            game->close(); 
            game_second->close();
        }
        if(game->pressed(keyboard::KEY_RIGHT))
        {
            rotationScale += .01;
            logger->print_message("RIGHT KEY PUSHED", debug::log::INFO);
        }
        if(game->pressed(keyboard::KEY_LEFT))
        {
            rotationScale -= .01;
            logger->print_message("LEFT KEY PUSHED", debug::log::INFO);
        }
        if(game->pressed(keyboard::KEY_R) && !game->pressed(keyboard::KEY_LEFT_SHIFT))
        {
            if(r != 1) { r += .01; }
            FoV += .1f;
            logger->print_message("R KEY PUSHED", debug::log::INFO);
        }
        if(game->pressed(keyboard::KEY_R) && game->pressed(keyboard::KEY_LEFT_SHIFT))
        {
            if(r != 0) { r -= .01; }
            FoV -= .1f;
            logger->print_message("R AND SHIFT KEY PUSHED", debug::log::INFO);         
        }
        if(game->pressed(keyboard::KEY_A) && !game->pressed(keyboard::KEY_LEFT_SHIFT))
        {
            if(a != 1) { a += .01; }
            logger->print_message("A KEY PUSHED", debug::log::INFO);
        }
        if(game->pressed(keyboard::KEY_A) && game->pressed(keyboard::KEY_LEFT_SHIFT))
        {
            if(a != 0) { a -= .01; }
            logger->print_message("A KEY AND SHIFT PUSHED", debug::log::INFO);
        }
        if(game->pressed(keyboard::KEY_G) && !game->pressed(keyboard::KEY_LEFT_SHIFT))
        {
            if(g != 1) { g += .01; }
        }
        if(game->pressed(keyboard::KEY_G) && game->pressed(keyboard::KEY_LEFT_SHIFT))
        {
            if(g != 0) { g -= .01; }
        }       
        if(game->pressed(keyboard::KEY_B) && !game->pressed(keyboard::KEY_LEFT_SHIFT))
        {
            if(b != 1) { b += .01; }
        }
        if(game->pressed(keyboard::KEY_B) && game->pressed(keyboard::KEY_LEFT_SHIFT))
        {
            if(b != 0) { b -= .01; }
        }       
        glBindVertexArray(vao3);    
        
        /*Modifies the actual object */
        glm::mat4 projectionMatrix  = glm::perspective(glm::radians(45.0f), static_cast<float>(640) / static_cast<float>(480), 0.1f, 100.0f);
        glm::mat4 translationMatrix = glm::translate(projectionMatrix, glm::vec3(0.0f, 0.0f, -3.0f));
        glm::mat4 rotationMatrix = glm::rotate(translationMatrix, glm::radians(34.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        // Camera matrix
        rotationMatrix = glm::rotate(translationMatrix, glm::radians(static_cast<float>(rotationScale*100)), glm::vec3(0.0f, static_cast<float>(0), 1.0f)); 
        /*camera */
        glm::mat4 View = glm::lookAt(
            glm::vec3(4,3,3), // Camera is at (4,3,3), in World Space
            glm::vec3(0,0,0), // and looks at the origin
            glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
            );
        glm::mat4 Model = glm::mat4(1.0f);
        
        glm::mat4 finalProjection = projectionMatrix *  View * Model;
        int finalTransformUniformLocation = glGetUniformLocation(shader, "finalTransform");
        glUniformMatrix4fv(finalTransformUniformLocation, 1, GL_FALSE, &finalProjection[0][0]); // Look through camera
        int redLocationId = glGetUniformLocation(shader, "red");
        glUniform1f(redLocationId,  r);
        int greenLocationId = glGetUniformLocation(shader, "green");
        glUniform1f(greenLocationId,  g);
        int blueLocationId = glGetUniformLocation(shader, "blue");      
        glUniform1f(blueLocationId,  b);
        int alphaLocationId = glGetUniformLocation(shader, "alpha");      
        glUniform1f(alphaLocationId,  a);
        int colorOnPosition = glGetUniformLocation(shader, "positionOffset");
        glUniform1i(colorOnPosition, 0);
        
        //  glDrawArrays(GL_TRIANGLES, 0, 12*3);
        //END OF FIRST INSTANCE
        // rotationMatrix = glm::rotate(translationMatrix, glm::radians(static_cast<float>(r*100)), glm::vec3(0.0f, 0.0f, 1.0f));
        // finalProjection = rotationMatrix * View * Model;
        // glUniformMatrix4fv(finalTransformUniformLocation, 1, GL_FALSE, &finalProjection[0][0]); // Look through camera
        // glDrawArrays(GL_TRIANGLES, 0, 12*3); //
        glBindVertexArray(0);
        if(game->pressed(keyboard::KEY_UP))
        {
            position += direction * deltaTime * speed;
        }
        // Move backward
        if (game->pressed(keyboard::KEY_DOWN))
        {
            position -= direction * deltaTime * speed;
        }
        // Strafe right
        if (game->pressed(keyboard::KEY_RIGHT))
        {
            position += right * deltaTime * speed;
        }
        // Strafe left
        if (game->pressed(keyboard::KEY_LEFT))
        {
            position -= right * deltaTime * speed;
        }
        glBindVertexArray(vao);    
        projectionMatrix = glm::perspective(glm::radians(FoV), 4.0f/3.0f, 0.1f, 100.0f);
        View = glm::lookAt(position, position+direction, up);
        finalProjection = projectionMatrix *  View * Model;
        finalTransformUniformLocation = glGetUniformLocation(shader, "finalTransform");
        glUniformMatrix4fv(finalTransformUniformLocation, 1, GL_FALSE, &finalProjection[0][0]); // Look through camera
        GLCall(glDrawArrays(GL_TRIANGLES, 0, ob->get_size()*3));
        // TODO(Bryce910): TRYING TO GET ERROR HANDLING ON THIS PIECE OF CODE
        //GLCall(glDrawElements(GL_TRIANGLES, 6, GL_INT,0));
    }
    
    // destroy "game"
    game->close();
    if(!game->is_running())
    {
        
        if(second_window.joinable())
        {
            second_window.detach();
            
        }
        if(i.joinable())
        {
            std::cout << "thread is joinable..." << std::endl;
            i.detach();
            std::cout << "thread is joined..." << std::endl;
        }
        else 
        {
            std::terminate();
            std::cout << "thread is not being terminated..." << std::endl;
        }
        
    }
    std::cout << "End the window...." << std::endl;
    return 0;
}
